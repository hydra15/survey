<?php

use App\Http\Controllers\Survey;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/', function () {
    return view('welcome');
});

Route::GET('/survey/{id?}', [Survey::class, 'index'])->name('survey');
Route::POST('/survey/', [Survey::class, 'update'])->name('update');