<?php

namespace App\Http\Controllers;

use App\Models\SingleTrace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Survey extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idQuestion = 0) {
        $data = 'default';

        if (Cache::has('survey')) {
            $data = Cache::get('survey');
            // $data = collect($data);
            // $json_encode = json_decode($data);
            // $data = $json_encode;
            // // dd($data);
        } else {
            $json = Http::get('https://the-trivia-api.com/api/questions')->json();
            $data = collect($json);
            $data = json_decode($data);
            Cache::put('survey', $data, 6000);
        }

        $singleTrace = new SingleTrace(0, 0, count($data));
        $singleTrace = $singleTrace->getCollectSingleTrace();
        $singleTrace = json_decode($singleTrace);
        // dd($singleTrace);
        
        $data = $data[$idQuestion];
        // dd($data);

        return view('index')->with(compact('data', 'singleTrace'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        if (Cache::has('survey')) {
            $data = Cache::get('survey');
            // $data = collect($data);
            // $json_encode = json_decode($data);
            // $data = $json_encode;
            // // dd($data);
        } else {
            $json = Http::get('https://the-trivia-api.com/api/questions')->json();
            $data = collect($json);
            $data = json_decode($data);
            Cache::put('survey', $data, 6000);
        }

        $singleTrace = new SingleTrace(0, 0, count($data));
        $singleTrace = $singleTrace->getCollectSingleTrace();
        $singleTrace = json_decode($singleTrace);

        // dd($singleTrace);

        $currentQuestion = $request->input('currentQuestion');

        $nQuestion = -1;
        if ($currentQuestion == $singleTrace->totalQuestions) {
            $nQuestion = 0;
        } else {
            $nQuestion = $currentQuestion;
        }

        // get params
        // dd($request->all());
        // get currentQuestion
        
        return redirect()->route('survey', ['id' => $nQuestion]);
    }
}
