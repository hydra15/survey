<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap demo</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    </head>
    <body>
        {{-- check if exist $data from redirect --}}
        @if (isset($data))
            {{-- nav --}}
            <nav class="navbar navbar-expand-lg bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Navbar</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"> Dropdown </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="dropdown-item" href="#">Action</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">Another action</a>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled">Disabled</a>
                            </li>
                        </ul>
                        <form class="d-flex" role="search">
                            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>
            {{-- ******************** --}}

            <br>

            {{-- progress --}}
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-label="Example with label" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
            </div>
            {{-- ******************** --}}

            <br>

            <div>

                {{-- questions --}}
                <div class="card">
                    <div class="card-header">
                        [001]
                        {{-- question --}}
                        {{$data->question}}
                        [AU - DO - ES - TI- EM]
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Categoria :: {{$data->category}}</h5>
                        <p class="card-text">Dificultad :: {{$data->difficulty}}</p>
                        {{-- input group --}}
                        <div id="blockResponses" class="input-group mb-3">
                            {{-- switch data->type (Multiple Choice-Single Choice) --}}
                            @if ($data->type == 'Multiple Choice')
                                @foreach ($data->incorrectAnswers as $singleAnswer)
                                    {{-- show dd singleAnswer --}}
                                    {{-- {{dd($data->incorrectAnswers[1])}} --}}
                                    {{-- {{dd($singleAnswer)}} --}}
                                    <div class="input-group mb-3">
                                        <div class="input-group-text">
                                            <input id="bResponse" name="001" class="form-check-input mt-0" type="checkbox" value="" aria-label="Checkbox for following text input">
                                        </div>
                                        <input type="text" disabled class="form-control" aria-label="Text input with checkbox" value="{{$singleAnswer}}">
                                    </div>
                                @endforeach
                            @elseif ($data->type == 'Single Choice')
                                <div class="input-group mb-3">
                                    <div class="input-group-text">
                                        <input id="bResponse" name="001" class="form-check-input mt-0" type="radio" value="" aria-label="Radio button for following text input">
                                    </div>
                                    <input type="text" disabled class="form-control" aria-label="Text input with checkbox" value="{{$singleAnswer}}">
                                </div>
                            @endif
                        </div>
                        {{-- ******************** --}}
                    </div>
                </div>
                {{-- ******************** --}}

            </div>

            <br>

            {{-- pagination --}}
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    {{-- foreach $singleTrace->totalQuestions --}}
                    @for ($i = 1; $i <= $singleTrace->totalQuestions; $i++)
                        <form action="{{route('update')}}" method="POST">
                            @csrf
                            <input type="hidden" name="currentQuestion" value="{{$i-1}}">
                            {{-- script, get responses from blockResponses, and sent in post --}}
                            <script>
                                function getResponses() {
                                    var responses = [];
                                    var blockResponses = document.getElementById('blockResponses');
                                    var bResponse = blockResponses.getElementsByTagName('input');
                                    for (var i = 0; i < bResponse.length; i++) {
                                        if (bResponse[i].checked) {
                                            responses.push(bResponse[i].value);
                                        }
                                    }
                                    return responses;
                                }
                            </script>
                            {{-- <li class="page-item"><button class="page-link" type="submit">{{$i}} </button></li> --}}
                            {{-- add event getResponses --}}
                            <li class="page-item"><button class="page-link" type="submit" onclick="getResponses()">{{$i}} </button></li>
                        </form>
                    @endfor
                    {{-- ******************** --}}
                </ul>
            </nav>
            {{-- ******************** --}}
        @else
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <h3>No Data Found</h3>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </body>
</html>