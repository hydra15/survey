<?php

namespace App\Models;

use Illuminate\Support\Collection;

class SingleTrace {
    private int $currentQuestion;
    private int $nextQuestion;
    private int $totalQuestions;
    private Collection $collectSingleTrace;

    public function __construct(int $currentQuestion, int $nextQuestion, int $totalQuestions = 0) {
        $this->currentQuestion = $currentQuestion;
        $this->nextQuestion = $nextQuestion;
        $this->totalQuestions = $totalQuestions;

        $this->collectSingleTrace = collect();

        $this->collectSingleTrace->put('currentQuestion', $this->currentQuestion);
        $this->collectSingleTrace->put('nextQuestion', $this->nextQuestion);
        $this->collectSingleTrace->put('totalQuestions', $this->totalQuestions);
    }

    public function getCurrentQuestion() {
        return $this->currentQuestion;
    }

    public function getNextQuestion() {
        return $this->nextQuestion;
    }

    public function getTotalQuestions() {
        return $this->totalQuestions;
    }

    public function setCurrentQuestion($currentQuestion) {
        $this->currentQuestion = $currentQuestion;
    }

    public function setNextQuestion($nextQuestion) {
        $this->nextQuestion = $nextQuestion;
    }

    public function setTotalQuestions($totalQuestions) {
        $this->totalQuestions = $totalQuestions;
    }

    public function getCollectSingleTrace() {
        return $this->collectSingleTrace;
    }
}
